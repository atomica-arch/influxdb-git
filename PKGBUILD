# Maintainer: Jason R. McNeil <jason@jasonrm.net>
# Contributor: Justin Dray <justin@dray.be>
# Contributor: Nicolas Leclercq <nicolas.private@gmail.com>
# Contributor: Charles B. Johnson <mail@cbjohnson.info>
# Contributor: Daichi Shinozaki <dsdseg@gmail.com>
# Contributor: Ben Alex <ben.alex@acegi.com.au>

_pkgname='influxdb'
pkgname="$_pkgname-git"
pkgver=v0.9.0.r77.ga78fef9
pkgrel=1
epoch=
pkgdesc='Scalable datastore for metrics, events, and real-time analytics'
arch=('i686' 'x86_64')
url='http://influxdb.org/'
license=('MIT')
groups=()
makedepends=('autoconf' 'protobuf' 'bison' 'flex' 'go' 'gawk' 'mercurial' 'git')
checkdepends=()

provides=('influxdb')
conflicts=('influxdb')
replaces=()
backup=('etc/influxdb.conf')
options=()
install="$_pkgname.install"
changelog=
source=(
  "git+https://github.com/influxdb/influxdb.git"
  "$_pkgname.service"
	"$_pkgname.install"
)
noextract=()
sha256sums=('SKIP'
  '0aca2dc1baff89c360d50894cdb2e0b87ed64165bb6f5d6a01d25ab3e7a7bd1e'
  'af143e04544197e6a66f6f4599f9cce8d03bad0c2e6fcbc01d68c300ec44748e'
)

pkgver() {
	cd "$_pkgname"
	git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

build() {
	export GOPATH="$srcdir"
	mkdir -p "$srcdir/src/github.com/influxdb"
	mv "$srcdir/influxdb" "$srcdir/src/github.com/influxdb/"
	cd "$srcdir/src/github.com/influxdb/influxdb"
	go get -u -f ./...
	go build ./...
	go install ./...

	sed -i 's|/var/opt|/var/lib|g' etc/config.sample.toml
}

check() {
  cd "$srcdir/src/github.com/influxdb/influxdb"
  # Required for testing
  go get github.com/davecgh/go-spew/spew
  go test -v ./... || true
}

package() {
  # systemctl service file
  install -D -m644  "$srcdir/influxdb.service" "$pkgdir/usr/lib/systemd/system/influxdb.service"

  # influxdb binary
  install -D -m755 "$srcdir/bin/influx" "$pkgdir/usr/bin/influx"
  install -D -m755 "$srcdir/bin/influxd" "$pkgdir/usr/bin/influxd"
  install -D -m755 "$srcdir/bin/urlgen" "$pkgdir/usr/bin/urlgen"

  # configuration file
  install -D -m644 "$srcdir/src/github.com/influxdb/influxdb/etc/config.sample.toml" "$pkgdir/etc/influxdb.conf"
}
